![Alt text](coditas-logo.gif?raw=true "Coditas Logo")

**Company Introduction**

Coditas is an off-shore product development organization housing a team of 550+ passionate engineers and designers. We engage in services spanning the entire software development life cycle. We have been providing quality solutions to multi-billion dollar brands across the globe, including DMart, ZestMoney, Symantec, Ambry Genetics, Tavisca, HDFC, and more. Coditas has experienced fast-paced growth thanks to an engineering-driven culture and steadfast philosophies around writing clean code, designing intuitive user experiences, and letting the work speak for itself.

**DevOps Assignment**

Below are two questions each of which have an OR option so you only need to answer any one of the options for each question. Finally we have mentioned a few expectations and potential answers on your approach towards the solution in a readme file.

**Q1-\> Any of the following**

  1. Dockerfile + Terraform + K8s - Take any sample go / java / nodejs / python application from the internet or build a simple one of your own. Write a dockerfile and package it into a docker image. Build the docker image and push it to the docker hub using terraform.
  2. Write a terraform code to create a 3-tier VPC and its components, create a K8s cluster.
  3. Extend the terraform code or provide an automation solution to create a k8s namespace - "co-labs", deploy 5 replicas of pods from the above built image in the "co-labs" namespace, and expose the deployment using service type LoadBalancer.

Or

1. Ansible + Terraform - Write a terraform code to create a VPC and its components, create an EC2 instance and install Jenkins on the EC2 instance using Ansible, run ansible command within/from terraform itself. Try to print the jenkins default password as an output.

**Q2 -\> Any of the following**

1. Terraform - Write a terraform code which will create a S3 Bucket, create a Cloudfront distribution and upload a simple "index.html" to the bucket. Output to the terraform code should be the cloudfront distribution URL and on hitting the URL, "index.html" should be served.

 OR

2. Python - Write a python code which takes a list of SSL secured domain names and port as an input, check if the SSL certificate is getting expired in less than 15 days, send an email or slack alert if the SSL certificate is getting expired in days \< 15.

**What are we expecting:**

- A Zip file or git repository which contains the solution of above problem statements and a readme file
- You should keep security of infrastructure resources at first place
- Clean code and best practices should be followed

**Please provide a readme file which answers the following:**

1. How long did you spend on the exercise, and if possible, short feedback about the exercise?

2. Detailed steps to execute the terraform code

3. What are all things you kept in mind from a security point of view when creating the infrastructure resources?

**Note:**

- You can use AWS or GCP or Azure as a cloud provider
- You can replace Terraform with Azure ARM Templates, GCP Cloud Deployment Manager or AWS Cloudformation if you are not familiar with Terraform and complete as much as you can
- This assignment is made keeping AWS in mind, you can replace AWS Service with GCP or Azure one if providing a solution for GCP or Azure. For example VPC to VNET in Azure, EC2 to Virtual Machines in Azure, Buckets to Storage Accounts in Azure.

**Submitting the solution**
- You can create a gitlab or github repository 
- Add varun-coditas (Gitlab) or varunelavia (Github) as collaborators to the repository.
- Reply to the email with link to your repository / send an email to varun.elavia@coditas.com with your updated resume
- OR send a Zip file to varun.elavia@coditas.com with your updated resume
